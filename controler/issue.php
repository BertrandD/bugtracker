<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($_GET['project']) && !empty($_GET['issue']))
{
	extract($_GET);
	$project = DBH::getUnique('Project',array('id' => $project));
	$issue = DBH::getUnique('Issue',array('id' => $issue));
	if($project!=null && $issue!=null)
	{
		if(!empty($_POST['state']) && is_numeric($_POST['state']))
		{
			$issue->setStateId($_POST['state']);
			DBH::save($issue);
		}
		if(!empty($_POST['content']))
		{
			extract($_POST);
			$newIssue = DBH::create('Comment',
				array(
					'issueId' => $issue->getId(),
					'userId' => $user->getId(),
					'content' => $content,
					'date' => date("Y-m-d H:i:s")
					)
				);
			if($newIssue!=null)
			{
				$ok = DBH::save($newIssue);
				if(!$ok)
					$tpl->value('erreur','Erreur lors de l\'enregistrement');
			}else{
				$tpl->value('erreur','Erreur lors de la création de l\'objet');
			}
		}


		$comments = DBH::getList('Comment',
									array(
										'issueId' => $issue->getId()
									),array(
										'date' => 'DESC'
									)
								);
		$nbComments=sizeof($comments);
		$state = $issue->getState();
		$tpl->value('projectId',$project->getId());
		$tpl->value('labelLabel',$state->getLabel());
		$tpl->value('nbComments',$nbComments);
		$tpl->value('labelName',$state->getName());
		$tpl->value('title',$issue->getTitle());
		$tpl->value('issueId',$issue->getId());
		$tpl->value('content',$issue->getContent());
		$tpl->value('issue',$tpl->build('small/issue'));

		$tplComment ="";
		foreach ($comments as $comment) {
			$tpl->value('content',$comment->getContent());
			$tpl->value('user',$comment->getUser()->getUsername());
			$tpl->value('date',$comment->getDate());
			$tplComment.=$tpl->build('small/comment');
		}	
	}
	$tpl->value('project',$project->getName());
	if(!empty($user)){
		if($project->getAdminId()==$user->getId())
		{
			$statesTpl ="";
			$states = DBH::getList('State');
			foreach ($states as $state) {
				$tpl->value('value',$state->getId());
				$tpl->value('text',$state->getName());
				$statesTpl .= $tpl->build('small/option');
			}
			$tpl->value('states',$statesTpl);
			$tpl->value('admin',$tpl->build('form/projectAdmin'));
		}else{
			$tpl->value('admin','');
		}


		$tpl->value('username',$user->getUsername());
		$tpl->value('addComment',$tpl->build('form/addComment'));
	}
	else
	$tpl->value('addComment',$tpl->build('form/login'));
}
$tpl->value('comments',$tplComment);
$page.=$tpl->build('issue');