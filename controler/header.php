<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(isset($_SESSION['id']))
{
	$identifier=array(
		'id' => $_SESSION['id']
		);
	$user=DBH::getUnique('User',$identifier);
}
$tpl->value('erreur','');

if(!empty($_POST['login']) && !empty($_POST['pass']))
{
	if(empty($_POST['newAccount']))
	{
		// Traitement des données
		$login = htmlspecialchars($_POST['login']);
		$pass = htmlspecialchars($_POST['pass']);

		$identifier=array(
			"username" => $login,
			"password" => sha1($pass),
			);
		$user = DBH::getUnique('User',$identifier);

		if(empty($user))
		{
			$tpl->value('erreur', "Identifiants incorrects");
		}else{
			$_SESSION['id'] = $user->getId();
		}
	}else{
		// Traitement des données
		$login = htmlspecialchars($_POST['login']);
		$pass = htmlspecialchars($_POST['pass']);

		$prevUser = DBH::getUnique('User',array('username' => $login));
		if(empty($prevUser))
		{
			$identifier=array(
				"username" => $login,
				"password" => sha1($pass),
				);
			$user = DBH::create('User',$identifier);

			if(empty($user))
			{
				$tpl->value('erreur', "Erreur lors de la création de l'utilisateur");
			}else{
				$_SESSION['id'] = $user->getId();
			}		
		}else{
			$tpl->value('erreur', "Nom d'utilisateur déjà pris");
		}
	}

}

if(isset($_POST['deconnexion']))
{
	session_destroy();
	$user=null;
}

if(!empty($user))
{
	$tpl->value('username',$user->getUsername());
	$tpl->value('header',$tpl->build('small/header'));
}else{
	$tpl->value('header','');
}