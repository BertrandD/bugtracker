<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

if(!empty($_GET['project']))
{
	extract($_GET);
	$project = DBH::getUnique('Project',array('name' => $project));
	if($project!=null)
	{

		if(!empty($_POST['title']) && !empty($_POST['title']))
		{
			extract($_POST);
			$newIssue = DBH::create('Issue',
				array(
					'title' => $title,
					'content' => $content,
					'solved' => 0,
					'projectId' => $project->getId(),
					'userId' => $user->getId(),
					'stateId' => State::$defaultId
					)
				);
			if($newIssue!=null)
			{
				$ok = DBH::save($newIssue);
				if(!$ok)
					$tpl->value('erreur','Erreur lors de l\'enregistrement');
			}else{
				$tpl->value('erreur','Erreur lors de la création de l\'objet');
			}
		}
		$issues = DBH::getList('Issue',array('projectId' => $project->getId()));

		$tplIssue ="";
		foreach ($issues as $issue) {
			$nbComments=DBH::getCount('Comment',array('issueId' => $issue->getId()));
			$state = $issue->getState();

			$tpl->value('labelLabel',$state->getLabel());
			$tpl->value('labelName',$state->getName());
			$tpl->value('title',$issue->getTitle());
			$tpl->value('issueId',$issue->getId());
			$tpl->value('content',$issue->getAbstract());
			$tpl->value('nbComments',$nbComments);
			$tpl->value('projectId',$project->getId());
			$tplIssue.=$tpl->build('small/issue');
		}	
		$tpl->value('listIssues',$tplIssue);
		$tpl->value('project',$project->getName());

		if(!empty($user)){
			$tpl->value('username',$user->getUsername());
			$tpl->value('reportIssue',$tpl->build('form/reportIssue'));
		}
		else
			$tpl->value('reportIssue',$tpl->build('form/login'));
		$page.=$tpl->build('project');
	}else{
		$page.=$tpl->build('accueil');	
	}
}else{
	$page.=$tpl->build('accueil');	
}
