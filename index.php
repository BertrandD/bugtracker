<?php
header('Content-Type: text/html; charset=utf-8');
session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL | E_STRICT);

define('PROJECT_DIR', realpath('./'));
define("FRONT_CONTROLER", "Yes I'm coming from front controler !");
define("CLASS_DIR", PROJECT_DIR."/model/classes/");
define("EXCEPTION_DIR", PROJECT_DIR."/model/classes/exception/");
define("CONTROLER_DIR", PROJECT_DIR."/controler/");
define("TPL_DIR", PROJECT_DIR."/templates/");
define("LIB_DIR", PROJECT_DIR."/lib/");

require_once EXCEPTION_DIR.'FrontControlerException.class.php';
require_once EXCEPTION_DIR.'NotSupportedMethodException.class.php';

require_once CLASS_DIR.'Database.class.php';
require_once CLASS_DIR.'DBH.class.php';
require_once CLASS_DIR.'Object.class.php';
require_once CLASS_DIR.'User.class.php';
require_once CLASS_DIR.'State.class.php';
require_once CLASS_DIR.'Project.class.php';
require_once CLASS_DIR.'Tag.class.php';
require_once CLASS_DIR.'Comment.class.php';
require_once CLASS_DIR.'Issue.class.php';
require_once CLASS_DIR.'Templates.class.php';

$tpl = new Templates;
$tpl->DEBUG = true; // Pour afficher les {{message}} qui ne sont pas remplacés par le code php, changer en true
$page = '';

	include(CONTROLER_DIR.'header.php');
	if (isset($_GET['op'])&&(file_exists(CONTROLER_DIR.$_GET['op'].'.php')))
	{
		include(CONTROLER_DIR.$_GET['op'].'.php');
	}
	else
	{
		include(CONTROLER_DIR.'accueil.php');
	}
	
	$tpl->value('titre', "BugTracker by Shellbash");
	$tpl->value('content',$page);

$generatedPage = $tpl->build('principal');

echo $generatedPage;
