<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = Issue
* @author = Bertrand Darbon
* @Descr = Sert d'objet Issue.
*/
class Issue extends Object {
	
	public static $table = 'Issue';

	protected $id; 			//int
	protected $name;		//string
	protected $label;	//string
	
	// Constructeur
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}
	
	// Method
	public function hydrate(array $data){
		foreach($data as $key=>$value){
			$methode = 'set'.ucfirst($key);
			if(is_callable(array($this, $methode))){
				$this->$methode($value);
			}
		}
	}

	public function isValide(){
		if($this->title != '')
			return true;
		else
			return false;
	}

	// Getters & Setters
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getLabel(){
		return $this->label;
	}

	public function setLabel($label){
		$this->label = $label;
	}
}	
?>
