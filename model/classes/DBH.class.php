<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
/**
 * @name = DAO
 * @author = Bertrand Darbon
 * @descr = Gestion des objets.
 */
abstract class DBH {

	/**
	* METHODES STATIQUES
	*/

	/**
	* @return l'objet de type $objectType ayant l'identifiant passé en paramètre
	* @Name : getUnique()
	* @Descr : Récupère un seul objet à partir des paramètres donnés
	* En passant getIdentity() comme identifier, vous etes certain de récupérer un objet
	* sinon aucune garantie, et surtout si plusieurs objets correspondent à l'identifier,
	* vous ne récupèrerez que le premier. Utiliser getList() si vous voulez en récupérer 
	* plusieurs ! 
	*/
	static public function getUnique($objectType, $identifier){

		// Init
		$object = NULL;
		$where='';
		$whereTable=array();

		if(!empty($objectType) && !empty($identifier)){
			self::getWhere($identifier,$where,$whereTable);

			// Mise en place de la requète
			// Un sécurité serait de mettre ORDER BY id pour être certain d'obtenir toujours le premier arrivant par ordre croissant
			// cependant les tables Dispose et Advice ne comporte pas d'id.
			// Techniquement l'id étant unique on ne devrait pas en avoir deux mais sait-ont jamais qu'un jour la table soit modifiée ou autre... :)
			$req = Database::getInstance()->prepare('SELECT * FROM '.$objectType::$table.$where.' LIMIT 1');
			$req->execute($whereTable);
			
			// Création de l'objet
			if($data = $req->fetch()){
				$object = new $objectType($data);
			}
		}
		return $object;
	}

	/**
	* @return un array() d'objets de type $objectType
	* @Name : getList()
	* @Descr : Récupère une liste d'objet
	*/
	static public function getList($objectType, $params = array(), $orders = array()){
		// Init
		$pTable = array();
		$reqWhereParameters = '';
		$reqOrder = '';
		$table = array(); // Tableau de retour

		if(!empty($objectType)){
			// Préparation des conditions
			
			self::getWhere($params, $reqWhereParameters, $pTable);
			
			// Préparation de l'ordre
			self::getOrderBy($orders,$reqOrder);
			
			// Requète
			$req = Database::getInstance()->prepare('SELECT * FROM '.$objectType::$table.$reqWhereParameters.$reqOrder);
			$req->execute($pTable);
			
			// Pour chaque données retournée
			while($data = $req->fetch()){
				$table[] = new $objectType($data);
			}
		}
		return $table;
	}

	/**
	* @author Leboc Philippe
	* @version 1.0
	*/
	static public function getCount($objectType, $identifier){
		// Init
		$object = NULL;
		$where='';
		$whereTable=array();

		if(!empty($objectType) && !empty($identifier)){
			self::getWhere($identifier,$where,$whereTable);

			$req = Database::getInstance()->prepare('SELECT count(*) as nbr FROM '.$objectType::$table.$where);
			try {
				$req->execute($whereTable);
			} catch (Exception $e) {
				printR($e->getMessage().'<br/>SELECT * FROM '.$objectType::$table.$where);
			}
			
			// Création de l'objet
			if($data = $req->fetch()){
				$object = $data['nbr'];
			}
		}
		return $object;
	}

	/**
	* @return l'objet de type $objectType ou NULL si une erreur s'est produite
	* @Name : create()
	* @Descr : Crée un nouvel objet
	* @Warn : pour récupérer un objet de la BDD, faire getUnique !!	 
	* @Warn : Ne le sauvegarde pas en base de donnée ! Pensez à appeller DAO::save() !!
	*/
	static public function create($objectType, array $params = array())
	{
		$object = NULL;

		try{
			$object = new $objectType($params);
		}catch (Exception $e){
			$object = NULL;
		}

		if(empty($object) || !$object->isValide()){
			$object = NULL;
		}

		return $object;
	}

	/**
	* @return void
	* @Name : removeObjectById()
	* @Descr : Supprime un objet de la base de donnée à partir de l'identifiant
	*/
	static public function removeObjectById($objectType, $id)
	{
		throw new NotSupportedMethodException();
	}

	/**
	* @return void
	* @Name : removeObject()
	* @Descr : Supprime un objet de la base de donnée directement à partir d'un objet
	*/
	static public function removeObject($object)
	{
		if(!empty($object)){
			self::removeObjectById(get_class($object),$object->getId());
		}
	}

	/**
	* @return true si l'objet à bien été sauvegard", false sinon
	* @Name : save()
	* @Descr : Enregistre un objet en bdd quelque soit son type
	* @Pre : $object doit respecter la structure expliquée dans Default.class.php
	*/
	static public function save($object){
		$result = false;

		if(!empty($object)){
			// Init
			$array=array();
			
			if($object->isValide()){
				// variables
				$objectType = get_class($object);
				$table = $objectType::$table;
				$where='';
				$whereTable=array();
				$SET="";

				self::getWhere($object->getIdentity(),$where,$whereTable);

				$req = Database::getInstance()->prepare('SELECT * FROM '.$table.$where);
				$req->execute($whereTable);
				
				if($data = $req->fetch()){ //Il y a bien une ligne avec cet id
					
					

					self::getSet($data,$object,$SET,$array);

					$req = Database::getInstance()->prepare('UPDATE '.$table.' SET '.$SET.$where);
					$req->execute(array_merge($array,$whereTable));
				}else{
					// IL va faloir créer une nouvelle ligne
					// $objectArray=(array)$object;
					$values="";
					$cols="";

					self::getColsAndValues($object,$cols,$values,$array);

					$req = Database::getInstance()->prepare('INSERT INTO '.$table.'('.$cols.') VALUES('.$values.')');
					$req->execute($array);
				}
				$result = true;
			}else{
				$result = false;
			}
		}
		return $result;
	}


	/**
	* Différents algorithme de génération des requetes
	*/
	
	/**
	* @return void
	* Ne permet que de faire des sélections type WHERE id=1 
	* Pas  possible de faire WHERE id<1
	*/
	static private function getWhere($params, &$p, &$pTable)
	{
		foreach($params as $param=>$value){
			if($p) $p .= ' AND ';
			if(is_array($value)){
				$p .= $param.' '.$value[0].' :'.$param;
				$pTable[':'.$param] = $value[1];
			}else{
				$p .= $param.' = :'.$param;
				$pTable[':'.$param] = $value;
			}
		}
		if($p) $p = ' WHERE '.$p;
	}

	/**
	* @return void
	* pour faire des ORDER BY id ASC AND name DESC par exemple
	* dans $o on trouverais donc dans ce cas :
	* $o = array(
	*	'id' => 'ASC',
	*	'name' => 'DESC'
	*	);
	*/
	static private function getOrderBy($orders,&$o)
	{
		foreach($orders as $order=>$value){
			if($o) $o .= ' AND ';
			$o .= $order.' '.$value;
		}
		if($o) $o = ' ORDER BY '.$o;
	}

	/**
	* @return void
	* Pour les updates, on récupère le SET nom='nouveauNom'
	*/
	static private function getSet($data, $object, &$SET, &$array)
	{
		foreach($data as $key=>$value){
			if(!is_numeric($key)){
				if($key!="id"){
					if($SET==""){
						$SET.=''.$key.'= :'.$key.'';
					}else{
						$SET.=', '.$key.'= :'.$key.'';
					}
				}
				$methodeGet = 'get'.ucfirst($key);
				if(is_callable(array($object, $methodeGet))){
					$array[':'.$key]=$object->$methodeGet();
				}else{
					$array[':'.$key]='"'.$value.'"';
				}
			}
		}
	}

	/**
	* @return void
	* Pour le insert, on récupère à la fois le nom des colonnes,
	* et les valeurs, dans le meme ordre pour éviter les erreurs
	*/
	static private function getColsAndValues($object,&$cols,&$values,&$array)
	{
		$objectArray=$object->iterate();
		$objectType=get_class($object);

		foreach($objectArray as $key=>$value){
			if($value!=''){
				$key=str_replace($objectType,'',$key);
				if(!is_numeric($key)){
					if($cols==""){
						$cols.='`'.$key.'`';
					}else{
						$cols.=', `'.$key.'`';
					}
					if($values==""){
						$values.=':'.$key;
					}else{
						$values.=', :'.$key;
					}
				}
				$methodeGet = 'get'.ucfirst($key);
				if(is_callable(array($object, $methodeGet))){
					$array[':'.$key]=$object->$methodeGet();
				}else{
					$array[':'.$key]='"'.$value.'"';
				}
			}
		}		
	}
}

?>
