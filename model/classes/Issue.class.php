<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = Issue
* @author = Bertrand Darbon
* @Descr = Sert d'objet Issue.
*/
class Issue extends Object {
	
	public static $table = 'Issue';
	private static $_state;

	protected $id; 			//int
	protected $title;		//string
	protected $content;	//string
	protected $solved;	//string
	protected $projectId;	//string
	protected $userId;	//string
	protected $stateId;	//string
	
	// Constructeur
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}
	
	// Method
	public function hydrate(array $data){
		foreach($data as $key=>$value){
			$methode = 'set'.ucfirst($key);
			if(is_callable(array($this, $methode))){
				$this->$methode($value);
			}
		}
	}

	public function isValide(){
		if($this->title != '')
			return true;
		else
			return false;
	}

	public function getState()
	{
		return DBH::getUnique('State',array('id' => $this->getStateId()));
	}


	public function getAbstract()
	{
		if(strlen($this->content) > 300)
		{
			return substr($this->content, 0,300).'...';
		}else
			return $this->content;
	}

	// Getters & Setters
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getTitle(){
		return $this->title;
	}

	public function setTitle($title){
		$this->title = $title;
	}

	public function getContent(){
		return $this->content;
	}

	public function setContent($content){
		$this->content = $content;
	}

	public function getSolved(){
		return $this->solved;
	}

	public function setSolved($solved){
		$this->solved = $solved;
	}

	public function getProjectId(){
		return $this->projectId;
	}

	public function setProjectId($projectId){
		$this->projectId = $projectId;
	}

	public function getUserId(){
		return $this->userId;
	}

	public function setUserId($userId){
		$this->userId = $userId;
	}

	public function getStateId(){
		return $this->stateId;
	}

	public function setStateId($stateId){
		$this->stateId = $stateId;
	}
}	
?>
