<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = User
* @author = Bertrand Darbon
* @Descr = Sert d'objet User.
*/
class User extends Object {
	
	public static $table = 'User';

	protected $id; 			//int
	protected $username;		//string
	protected $password;	//string
	
	// Constructeur
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}
	
	// Method
	public function hydrate(array $data){
		foreach($data as $key=>$value){
			$methode = 'set'.ucfirst($key);
			if(is_callable(array($this, $methode))){
				$this->$methode($value);
			}
		}
	}

	public function isValide(){
		if($this->name != '')
			return true;
		else
			return false;
	}

	// Getters & Setters
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getUsername(){
		return $this->username;
	}

	public function setUserName($username){
		$this->username = $username;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setPassword($password){
		$this->password = $password;
	}
}	
?>
