<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
 
abstract class Object{
	
	/*
	*	@Name : isValide()
	*	@Descr : Nécessaire pour l'enregistrement en base de donnée, permet de vérifier
	*			qu'un objet dispose bien des attributs indispensable.
	*			Au minimum elle doit contenir pour ne pas avoir d'erreur sql au niveau des clés:
	public function isValide(){
		if($this->id!='')
			return true;
		else
			return false;
	}		
	*/
	public abstract function isValide();


	/*
	* @Name : getIdentity()
	* @Descr : Définit ce qui fait de l'objet son identité,
	*			ie : ce qui l'identifie aux autres / sa clé primaire dans la BDD
	* @Return : Un tableau clé => valeur
	Exemple :
			}
	*/
	public function getIdentity()
	{
		return array(
				"id" =>$this->id
			);
	}



	/* 
	 * @Name : iterate()
	 * @Descr : Renvoie un tableau avec toutes les valeurs de l'objet
	 */
	public function iterate(){
		return get_object_vars($this);
	}


}
