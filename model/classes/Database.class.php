<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
/**
* Class Database
* @author Leboc Philippe
* @version 1.0
* @desc Sert à la gestion de la base de donnée, c'est une classe statique.
* 
* Utilisation :
* $statement = Database::getInstance()->prepare("SELECT * FROM users WHERE id=? AND ville=? LIMIT ?");
* if($statement) cette ligne est un simple check mais vous pouvez l'omettre.
* {
*   // Il n'est imposé de traiter le bindValue de cette manière.
*	// Le PDO::PARAM_ est optionel mais je vous invite fortement à le mettre.
*	$statement->bindValue(1, 10, PDO::PARAM_INT);			// premier "?"
* 	$statement->bindValue(2, "Grenoble", PDO::PARAM_STR);	// second "?"
*	$statement->bindValue(3, 100, PDO::PARAM_INT);			// dernier "?"
*	$statement->execute();
* }
* Ensuite traité le retour grace à if($resultat = $statement->fetch())
*/
abstract class Database{
	/**
	* @author Leboc Philippe
	* @version 1.0
	* Attributs privés
	*/
	private static $_instance = null;
	

	/**
	* @author Leboc Philippe
	* @version 1.0
	* A quel moment ferme-t-ont la connexion à la base de données ?
	*/
	public function close()
	{
		if(!is_null($_instance))
		{
			$_instance = null;
		}
	}

	/**
	* @author Leboc Philippe
	* @version 1.0
	* Permet d'avoir une instance UNIQUE pour la connexion à la base de données.
	*/
	public static function getInstance(){
		if(is_null(self::$_instance))
		{		
			try{
				//self::$_instance = new PDO('sqlite:temp.db'); // SQLITE à Modifier.
				self::$_instance = new PDO('mysql:host=lesmordusduclavier.fr;dbname=bertrand_bugtracker',"bugtracker05", "LhvGHJFEKhD4DcFJ", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				self::$_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
			}catch(PDOException $e){
				die('Une erreur est survenue lors de l\'initialisation à la base de données : '.$e->getMessage());
			}
		}
		return self::$_instance;
	}
}