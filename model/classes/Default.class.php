<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
/*************************************************************************************************
* WARNING : cet objet n'a d'autre utilité que d'expliquer la structure indispensable des objets  *
*			Elle n'est donc pas à instancier ! De toute façon php vous crieras dessus :P         *
**************************************************************************************************/

/**
* Classe d'exemple de base de ce qui est requis pour un objet
* NB : chaque attribut doit absolument disposer d'un getter
* et d'un setter.
*/
class Default // ce nom est réservé par php, n'essayez surtout pas d'inclure ce fichier qq part :P
{
	static $table = "tableName";

	protected $attribute;

	/**
	*	Constructeur, rarement besoin de le modifié
	* Pour créer un objet on passe les valeurs des attributs en paramètres
	* sous forme d'un tableau
	*/
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}

	/**
	* Nécessaire pour le constructeur
	*/
	public function hydrate(array $data){
		foreach($data as $key=>$value){
			$methode = 'set'.ucfirst($key);
			if(is_callable(array($this, $methode))){
				$this->$methode($value);
			}
		}
	}

	/**
	*	@Name : isValide()
	*	@Descr : Nécessaire pour l'enregistrement en base de donnée, permet la vérification par le DAO
	*			qu'un objet dispose bien des attributs indispensable (clé primaire).
	*/
	public function isValide(){
		if($this->attribute!='')
			return true;
		else
			return false;
	}	

	public function getAttribute()
	{
		return $this->attribute;
	}

	public function setAttribute($attr)
	{
		$this->attribute=$attr;
	}
}