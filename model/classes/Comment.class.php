<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = Comment
* @author = Bertrand Darbon
* @Descr = Sert d'objet Comment.
*/
class Comment extends Object {
	
	public static $table = 'Comment';

	protected $id; 			//int
	protected $issueId;		//int
	protected $userId;	//int
	protected $content;	//string
	protected $date;	//datetime
	
	// Constructeur
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}
	
	// Method
	public function hydrate(array $data){
		foreach($data as $key=>$value){
			$methode = 'set'.ucfirst($key);
			if(is_callable(array($this, $methode))){
				$this->$methode($value);
			}
		}
	}

	public function isValide(){
		if($this->content != '' && $this->issueId != '' && $this->userId != '')
			return true;
		else
			return false;
	}

	public function getUser()
	{
		return DBH::getUnique('User',array('id' => $this->getUserId()));
	}

	// Getters & Setters
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getIssueId(){
		return $this->issueId;
	}

	public function setIssueId($issueId){
		$this->issueId = $issueId;
	}

	public function getUserId(){
		return $this->userId;
	}

	public function setUserId($userId){
		$this->userId = $userId;
	}

	public function getContent(){
		return $this->content;
	}

	public function setContent($content){
		$this->content = $content;
	}

	public function getDate(){
		return $this->date;
	}

	public function setDate($date){
		$this->date = $date;
	}
}	
?>
