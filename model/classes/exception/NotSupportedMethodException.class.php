<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}
/**
* Exception levée quand un script php n'est pas appelée depuis le controleur frontal
*/
class NotSupportedMethodException extends Exception
{
	function __construct()
	{
		parent::__construct(_("Cette méthode n'a pas encore été implémentée"));
	}
}