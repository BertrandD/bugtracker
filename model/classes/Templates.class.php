<?php
if(!defined("FRONT_CONTROLER"))
{
    throw new FrontControlerException();
}

/*
Pour utiliser le système de boucle (1 boucle par template) : 
------------------------------------------------------------
$tpl->getboucle("nomdutemplate"); <-- charge le template pour gérer la boucle
while/for/... {
$tpl->value("nom de l'élément", "valeur"); <-- définit les {{xxx}} à remplacer dans la boucle
$tpl->value("nom de l'élément2", "valeur2");
$tpl->boucle(); <-- fin du premier passage dans la boucle
}
$tpl->value("nom de l'élément", "valeur"); <-- valeurs à remplacer à l'éxterieur de la boucle
$tpl->value("nom de l'élément2", "valeur2");
$xxx = $tpl->finboucle(); <-- finalisation du template complet
*/

function translate($string)
{
    $string[0]=str_replace("[[", "", $string[0]);
    $string[0]=str_replace("]]", "", $string[0]);
    return _($string[0]);
}

class templates 
{
    public $DEBUG = false;
    var $template = array();

    public function value($nom, $valeur)
    {
        $this->template[$nom] = $valeur;
    }

    public function parsetemplate($templatepage)
    {
        global $template;
            mb_internal_encoding('UTF-8');
        foreach($this->template as $a => $b) 
		{
            $templatepage = str_replace("{{{$a}}}", $b, $templatepage);
        }
        $templatepage = preg_replace_callback("#\[\[.+\]\]#", "translate", $templatepage);
        if(!$this->DEBUG)
            $templatepage = preg_replace("#\{\{.+\}\}#", "", $templatepage);
        return $templatepage;
    }
    
    public function build($nom)
    {
        global $userrow;
		global $controlrow;

			$fichier = TPL_DIR.''. $nom . '.html';

		
		$templatepage = file_get_contents($fichier);

        $page = templates::parsetemplate($templatepage);

        return $page;
    }

    public function error($error) // Dans le cas ou on tombe sur une erreur pendant le jeu on renvoye ici ce qui permet de mettre une mise en forme au erreur 
    {
		unset($this->template); // On vide le tableau des templates
		$this->value('afferreurs', $error); // On envoye l'erreur sur le template
		//echo $this->construire('afferreurs'); // On envoye le template montrant l'erreur
		die(); // On stoppe le programme
    }
}

?>