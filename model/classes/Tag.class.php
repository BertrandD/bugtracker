<?php
if(!defined("FRONT_CONTROLER"))
{
	throw new FrontControlerException();
}

/**
* @Name = Tag
* @author = Bertrand Darbon
* @Descr = Sert d'objet Tag.
*/
class Tag extends Object {
	
	public static $table = 'Tag';

	protected $id; 			//int
	protected $tag;		//string
	
	// Constructeur
	public function __construct(array $data = array()){
		if(!empty($data))
			$this->hydrate($data);
	}
	
	// Method
	public function hydrate(array $data){
		foreach($data as $key=>$value){
			$methode = 'set'.ucfirst($key);
			if(is_callable(array($this, $methode))){
				$this->$methode($value);
			}
		}
	}

	public function isValide(){
		if($this->tag != '')
			return true;
		else
			return false;
	}

	// Getters & Setters
	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getTag(){
		return $this->tag;
	}

	public function setTag($tag){
		$this->tag = $tag;
	}
}	
?>
